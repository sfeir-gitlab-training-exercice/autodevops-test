### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

This is a test

### Connecting a Kubernetes cluster with Auto DevOps

To connect Gitlab to the Kubernetes, we use the Gitlab agent.

Here are the steps to follow:  

1. add an agent configuration to the project:  

In the repository, create a directory in this location : .gitlab/agents/${agentName}.  ( the agent-name must meet the [naming convention](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#agent-naming-convention). )

In the directory, create a **config.yaml** file. (Ensure the filename ends in .yaml, not .yml).
Add content to the config.yaml file:
For a GitOps workflow, view the  [configuration reference](https://docs.gitlab.com/ee/user/clusters/agent/gitops.html#gitops-configuration-reference) for details.
For a GitLab CI/CD workflow, view the [configuration reference](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_workflow.html) for details.

**For our lab we use a "CI/CD workflow"**. 

2. register the agent:  

From the left sidebar, select **Infrastructure** > **Kubernetes** clusters.
Select **Connect a cluster (agent)**.
If you want to create a configuration with CI/CD defaults, type a name that meets the naming convention.
select the agent created from the list.
Select Register an agent.
GitLab generates an access token for the agent. Securely store this token. You need it to install the agent in your cluster and to update the agent to another version.
Copy the command under Recommended installation method. You need it when you use the one-liner installation method to install the agent in your cluster.  

3. Install the agent in your cluster.

Connect to your Cluster and install the agent with Helm (use the command you copied when you registered your agent with GitLab).

4. add Gitlab Variables to projects:

- KUBE_CONTEXT : ${path/to/agent/project}:${agent-name}
From GitLab 14.5, can be used to select a context to use from KUBECONFIG. When KUBE_CONTEXT is blank, the default context in KUBECONFIG (if any) is used. A context must be selected when used with the agent for Kubernetes.

- KUBE_INGRESS_BASE_DOMAIN: to retrieve from the cluster
used to set a domain per cluster. See cluster domains for more information.

